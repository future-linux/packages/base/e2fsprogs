# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=(e2fsprogs fuse2fs)
pkgbase=e2fsprogs
pkgver=1.46.5
pkgrel=2
pkgdesc="Ext2/3/4 filesystem utilities"
arch=('x86_64')
url="http://e2fsprogs.sourceforge.net"
license=('GPL' 'LGPL' 'MIT')
makedepends=('systemd' 'util-linux' 'fuse')
source=(https://www.kernel.org/pub/linux/kernel/people/tytso/${pkgbase}/v${pkgver}/${pkgname}-${pkgver}.tar.gz)
sha256sums=(b7430d1e6b7b5817ce8e36d7c8c7c3249b3051d0808a96ffd6e5c398e4e2fbb9)

prepare() {
    cd ${pkgbase}-${pkgver}

    # Remove unnecessary init.d directory
    sed -i '/init\.d/s|^|#|' misc/Makefile.in

    mkdir -v build
}

build() {
    cd ${pkgbase}-${pkgver}/build

    ${build_configure}        \
	--sbindir=/usr/sbin   \
        --with-root-prefix='' \
        --sysconfdir=/etc     \
        --enable-elf-shlibs   \
        --disable-libblkid    \
        --disable-libuuid     \
        --disable-uuidd       \
        --disable-fsck

    make

    # regenerate locale files
    find po/ -name '*.gmo' -delete
    make -C po update-gmo
}

package_e2fsprogs() {
    depends=('bash' 'util-linux')
    backup=(etc/mke2fs.conf
        etc/e2scrub.conf)

    cd ${pkgbase}-${pkgver}/build

    unset MAKEFLAGS

    make DESTDIR=${pkgdir} install install-libs

    sed -i -e 's/^AWK=.*/AWK=awk/' ${pkgdir}/usr/bin/compile_et

    # remove references to build directory
    sed -i -e 's#^DIR=.*#DIR="/usr/share/ss"#' ${pkgdir}/usr/bin/mk_cmds
    sed -i -e 's#^DIR=.*#DIR="/usr/share/et"#' ${pkgdir}/usr/bin/compile_et

    # remove static libraries with a shared counterpart
    rm ${pkgdir}/usr/lib64/lib{com_err,e2p,ext2fs,ss}.a

    # remove fuse2fs which will be packaged separately
    rm ${pkgdir}/usr/{bin/fuse2fs,share/man/man1/fuse2fs.1}

}

package_fuse2fs() {
    pkgdesc='Ext2/3/4 filesystem driver for FUSE'
    depends=('fuse' 'e2fsprogs')

    cd ${pkgbase}-${pkgver}/build

    install -D -m0755 misc/fuse2fs ${pkgdir}/usr/bin/fuse2fs
    install -D -m0644 misc/fuse2fs.1 ${pkgdir}/usr/share/man/man1/fuse2fs.1
}
